from django.db import models

class Habit(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=250)
    goal = models.IntegerField(default=0, null=False)
    monday = models.BooleanField(default=False, null=False)
    tuesday = models.BooleanField(default=False, null=False)
    wednesday = models.BooleanField(default=False, null=False)
    thursday = models.BooleanField(default=False, null=False)
    friday = models.BooleanField(default=False, null=False)
    saturday = models.BooleanField(default=False, null=False)
    sunday = models.BooleanField(default=False, null=False)
    progress = models.IntegerField(default=0, null=False)
    
    def __str__(self):
        return f'{self.name} {self.description} {self.goal}'
