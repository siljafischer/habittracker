from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

from datetime import datetime

from .models import Habit


def index(request):
    habits = Habit.objects.all()
    for habit in habits:
        habit.progress = summe_berechnen(habit)
        habit.save()
    return render(request, 'habitapp/index.html', {'habits': habits})

def delete(request):
    habits = Habit.objects.all()
    for habit in habits:
        habit.monday = False
        habit.tuesday = False
        habit.wednesday = False
        habit.thursday = False
        habit.friday = False
        habit.saturday = False
        habit.sunday = False
        habit.progress = 0
        habit.save()
    return render(request, 'habitapp/index.html', {'habits': habits})
    


def summe_berechnen(habit):
    summe = 0
    if habit.monday:
        summe += 1
    if habit.tuesday:
        summe += 1
    if habit.wednesday:
        summe += 1
    if habit.thursday:
        summe += 1
    if habit.friday:
        summe += 1
    if habit.saturday:
        summe += 1
    if habit.sunday:
        summe += 1    
    return summe


def update_habit_def(habit, post):
    habit.monday = "habit.monday" in post  
    habit.tuesday = "habit.tuesday" in post
    habit.wednesday = "habit.wednesday" in post
    habit.thursday = "habit.thursday" in post
    habit.friday = "habit.friday" in post
    habit.saturday = "habit.saturday" in post
    habit.sunday = "habit.sunday" in post
    habit.save()


def update_habit(request, habit_id):
    habit = get_object_or_404(Habit, pk=habit_id)
    if request.POST:
        update_habit_def(habit, request.POST)
        return HttpResponseRedirect(reverse('habitapp:index'))
    context = {
            "habit": habit, 
            }
    return render(request, "habitapp/index.html", context)