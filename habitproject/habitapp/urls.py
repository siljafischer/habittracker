from django.contrib import admin
from django.urls import path, include

from . import views

app_name='habitapp'
urlpatterns = [
    path('', views.index, name='index'),
    path('update_habit/<int:habit_id>', views.update_habit, name='update_habit'),
    path('delete/', views.delete, name='delete'),
]
